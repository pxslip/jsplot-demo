/**
 * jsPlot (c) 2014 Will Kruse
 * jsPlot is a web-based replacement for xPlot or jPlot. In theory this will run on any server, or locally
 * NOTE: at the time of creation only some browsers support the File api
 *
 * jsPlot is released under the MIT License whose text can be found in the included LICENSE file
 */
/*jshint camelcase: false, unused: false */
/*globals PlotData, CanvasHandler, AboveText, BelowText, RightText, LeftText, CenterText, Line, DemarcatedLine, Diamond, Box, DownTick, UpTick, LeftTick, RightTick, HorizTick, VertTick, UpArrow, DownArrow, LeftArrow, RightArrow, Invisible, Dot, Plus, Cross, Color*/

//init function on page load
(function() {
	'use strict';
	//set up the canvas to be used once the data has been read
	//wait for a file to be selected
	document.getElementById('plot_file').addEventListener('change',
		function(evt) {
			var files, reader;
			PlotData.reset();
			files = evt.target.files;//read the file list from the input element #plot_file
			reader = new FileReader();
			reader.onload = function(file) {
				CanvasHandler.init('plot', file.target.result);
			};
			reader.readAsText(files[0]);//convert the File reference into a string
		}, false);
})();


